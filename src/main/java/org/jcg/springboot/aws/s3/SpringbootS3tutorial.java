package org.jcg.springboot.aws.s3;

import java.io.File;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

/**
 * Main implementation class which serves two purpose in a spring boot application: Configuration and bootstrapping.
 * @author yatin-batra
 */
@SpringBootApplication
public class SpringbootS3tutorial {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringbootS3tutorial.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringbootS3tutorial.class, args);
		LOGGER.info("SpringbootS3tutorial application started successfully.");
	}
	
}
