package org.jcg.springboot.aws.s3.ctrl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.jcg.springboot.aws.s3.entity.AWSNode;
import org.jcg.springboot.aws.s3.serv.AWSS3Service;
import org.jcg.springboot.aws.s3.serv.AWSS3ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.Response;
import com.amazonaws.SdkClientException;
import org.springframework.core.env.Environment;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@RestController
@RequestMapping(value= "/s3")
public class AWSS3Ctrl {

	@Autowired
	private AWSS3Service service;
	
	@Autowired
	private AmazonS3 s3;
	
	@Value("${aws.s3.bucket}")
	private String bucketName;
	
	@Autowired 
	private Environment env;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3Ctrl.class);
	
	
	
	@CrossOrigin
	@PostMapping(value= "/upload")
	public Map<String, Object> uploadFile(@RequestPart(value= "file") final MultipartFile multipartFile, @RequestParam(value="keyName") String keyName) {
		service.uploadFile(multipartFile,keyName);
		Map<String, Object> map = new HashMap<>();
		map.put("url", "");
		map.put("status", HttpStatus.OK);
		map.put("fullPath", keyName+"/"+multipartFile.getOriginalFilename());
		System.out.println(keyName+"/"+multipartFile.getOriginalFilename());
		return map;
	}
	
	
	@CrossOrigin
	@GetMapping(value= "/renameObject")
	public Map<String, Object> renameObject(@RequestParam(value="sourceKey") String sourceKey,
			@RequestParam(value="destinationKey") String destinationKey) {
		Map<String, Object> map = new HashMap<>();
		try {
            // Copy the object into a new object in the same bucket.
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, sourceKey, bucketName, destinationKey);          
            s3.copyObject(copyObjRequest);
            // delete original one
            s3.deleteObject(bucketName, sourceKey);
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
        	map.put("status", HttpStatus.BAD_REQUEST);
			map.put("msg", "aws error");
            e.printStackTrace();
            return map;
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client  
            // couldn't parse the response from Amazon S3.
        	map.put("status", HttpStatus.BAD_REQUEST);
        	map.put("msg", "sdk error");
            e.printStackTrace();
            return map;
        }
		map.put("status", HttpStatus.OK);
		map.put("msg", "rename success");
		return map;
	}
	
	
	
	@CrossOrigin
	@GetMapping(value= "/createFolder")
	public Map<String, Object> createFolder(@RequestParam(value="folderName") String folderName) {
		createFolder(bucketName,folderName, s3);
		Map<String, Object> map = new HashMap<>();
		map.put("status", HttpStatus.OK);
		return map;
	}
	
	
	 public static void createFolder(String bucketName, String folderName, AmazonS3 s3) {
	        // create meta-data for your folder and set content-length to 0
	        ObjectMetadata metadata = new ObjectMetadata();
	        metadata.setContentLength(0);
	        // create empty content
	        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
	        // create a PutObjectRequest passing the folder name suffixed by /
	        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
	                folderName + "/", emptyContent, metadata);
	        // send request to S3 to create folder
	        s3.putObject(putObjectRequest);
	    }
	
	
	
	
	
     /**
      * this is the method to delete single file
      * @param keyName
      * @return
      */
	
	@CrossOrigin
	@GetMapping(value= "/delete")
	public Map<String, Object> deleteFile(@RequestParam(value="keyName") String keyName) {
		 Map<String, Object> map = new HashMap<>();
		 try {
	            s3.deleteObject(bucketName, keyName);
	    		map.put("status", HttpStatus.OK);
	    		map.put("msg", "file deleted");
	        } catch (AmazonServiceException e) {
	            System.err.println(e.getErrorMessage());
	    		map.put("status", HttpStatus.BAD_REQUEST);
	    		map.put("msg", "file deletion failed");
	        }	
		return map;
	}
	
	
	/**
	 * this is the method to batch delete files
	 * @param keyNames
	 * @return
	 */
	
	@CrossOrigin
	@GetMapping(value= "/batchDelete")
	public Map<String, Object> batchDelete(@RequestParam(value="nodeKeyNames") String[] keyNames) {
		 Map<String, Object> map = new HashMap<>();  
		  try {
	            DeleteObjectsRequest dor = new DeleteObjectsRequest(bucketName)
	                    .withKeys(keyNames);
	            s3.deleteObjects(dor);
	        	map.put("status", HttpStatus.OK);
	    		map.put("msg", "file deleted successfully");
	        } catch (AmazonServiceException e) {
	        	map.put("status", HttpStatus.BAD_REQUEST);
	    		map.put("msg", "file deletion failed");
	        }  
		return map;
	}
	
	/**
	 * this is the method to download single file
	 * @param keyName
	 * @return
	 */	
    @CrossOrigin
    @GetMapping(value= "/download")
    public String downloadFile(HttpServletResponse response, @RequestParam(value= "keyName") final String key
    ) {
        Map<String, Object> map = new HashMap<>();
        S3Object fullObject = null;
        try {
            
            fullObject = s3.getObject(new GetObjectRequest(bucketName, key));
            InputStream objectData = fullObject.getObjectContent();
            response.reset();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + key);
            response.setHeader("Access-Control-Allow-Origin", "*");

            OutputStream out = null;
            byte[] buffer = new byte[1024];
            BufferedInputStream bis = null;
            try {
                bis = new BufferedInputStream(objectData);    
                byte[] buff =new byte[1024];
                int index=0;
                out = response.getOutputStream();
                while((index= bis.read(buff))!= -1){
                    out.write(buff, 0, index);
                    out.flush();
                }
                return "download succ";
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (objectData != null) {
                    try {
                        objectData.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
        	System.out.println("aws error");
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
        return "download failed";
    }


	@CrossOrigin
	@GetMapping(value= "/list")
	public Map<String, Object> listFile(@RequestParam("userName") String userName){
	    Map<String, Object> map = new HashMap<>();
	    ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(userName+"/");
	    
	    ListObjectsV2Result result = s3.listObjectsV2(req);
	    List<S3ObjectSummary> objects = result.getObjectSummaries();
	    if(objects.size()==0)
	    {
	    	//create new root folder
	    	createFolder(bucketName,userName, s3);
	    }
	        
	    List<AWSNode> treeList = new ArrayList<AWSNode>();
	    map.put("bucketName", bucketName);
	    map.put("JsonTree", buildZtree(objects,treeList));
	    return map;
	}
	
	
	
	private List<AWSNode> buildZtree(List<S3ObjectSummary> objects, List<AWSNode> treeList) {
	    int pId = 1;
	    int id = 1;
	    Map<String, Integer> parentMap = new HashMap<>();
	    Set<String> folderSet = new TreeSet<>();
	    List<String> fileList = new ArrayList<>();
	    for (int i=0; i< objects.size(); i++) {
	      S3ObjectSummary o = objects.get(i);
	      //logger.info("obj "+i+" = "+JSONObject.toJSONString(o));
	      if(!o.getKey().endsWith("/")){
	        fileList.add(o.getKey());
	        String[] pathList = o.getKey().split("/");
	        if(pathList.length>1){
	          String basePath = pathList[0] + "/";
	          folderSet.add(basePath);
	          for(int j=1; j<pathList.length-1; j++){
	            basePath = basePath + pathList[j] + "/";
	            //logger.info("basePath = "+basePath);
	            folderSet.add(basePath);
	          }// add path to set
	        }
	      } else {
	        folderSet.add(o.getKey());
	      }
	    }// loop
	    int count = 0;
	    for (String o : folderSet) {
	            //System.out.println("count = "+count++ +" o = "+o);
	            AWSNode node = new AWSNode();
	            pId = id;
	            node.setId(pId);
	            String[] names = o.split("/");
	            parentMap.put(o.substring(0, o.lastIndexOf("/")), id);
	            if(names.length>1)
	            {
	                String parentName = o.substring(0, o.lastIndexOf('/',o.lastIndexOf("/")-1));
	                node.setpId(parentMap.get(parentName));
	            }else
	            {
	                node.setpId(pId);
	            }
	            node.setName(names[names.length-1]);
	            node.setIsParent(true);
	            node.setKeyName(o);
	            treeList.add(node);
	            id++;
	    }
	    for(String o: fileList){
	        id++;
	        AWSNode node = new AWSNode();
	        node.setId(id);
	        String[] names = o.split("/");
	        if(names.length>1)
	        {
	          node.setpId(parentMap.get(o.substring(0, o.lastIndexOf("/"))));
	        }else if (names.length==1) {
	          node.setpId(1);
	        }else
	        {
	          node.setpId(pId);
	        }
	        node.setName(names[names.length-1]);
	        node.setIsParent(false);
	        node.setKeyName(o);
	        treeList.add(node);
	    }
	    return treeList;
	}
}
