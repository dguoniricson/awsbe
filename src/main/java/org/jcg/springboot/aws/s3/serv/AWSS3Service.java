package org.jcg.springboot.aws.s3.serv;

import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;

public interface AWSS3Service {

	void uploadFile(MultipartFile multipartFile, String keyName);
	
	byte[] downloadFile(String keyName);
	
	void createFolder(String bucketName, String folderName, AmazonS3 s3);
}
